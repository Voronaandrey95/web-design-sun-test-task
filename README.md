# web design sun Test Task

## Requirements

To run this project, you need to have Node.js installed on your computer. We recommend using version 18.14.0 or later.

## Installation

To install this project, please follow these steps:

1.  Clone this repository to your local machine.
2.  Navigate to the project's root directory in your terminal or command prompt.
3.  Run the following command to install the necessary packages:

```shell script
npm install
```

4. Run the following command to initialize the project:

```shell script
  npm init
```

## Usage

To start the application, run the following command in your terminal or command prompt:

```shell script
  npm run dev
```

This will start the application and open it in your default browser. If it does not automatically open, you can navigate to http://localhost:3000 to view it.
