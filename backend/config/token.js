export const TOKEN_DATA = {
  ACCESS: {
    expiresIn: 60 * 60,
  },
};

export const TOKEN_USER_DATA = (id, name) => {
  return {
    userId: id,
    name: name,
  };
};
