export const config = {
  HOST: "localhost",
  PORT: 8000,
  DBUSER: "db-user",
  DBPASSWORD: "db-password",
  DBNAME: "db-name",
  JWT: "your key", //JWT key
};
