import { conn } from "../../app.js";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import { config } from "../../config/config.js";
import { TOKEN_DATA, TOKEN_USER_DATA } from "../../config/token.js";

export const getReviews = async (req, res) => {
  const page = parseInt(req.params.page) || 1;
  const limit = parseInt(req.params.limit) || 10;
  const offset = (page - 1) * limit;

  const countQuery = "SELECT COUNT(*) as count FROM reviews";
  conn.query(countQuery, (err, result) => {
    if (err) {
      return res.status(500).json({ error: "Something went wrong" });
    }

    const count = result[0].count;
    const totalPages = Math.ceil(count / limit);

    const query = `SELECT * FROM reviews LIMIT ${limit} OFFSET ${offset}`;
    conn.query(query, (err, result) => {
      if (err) {
        return res.status(500).json({ error: "Something went wrong" });
      }

      const next = page < totalPages ? page + 1 : null;
      const prev = page > 1 ? page - 1 : null;

      return res.status(200).json({
        data: result,
        currentPage: page,
        nextPage: next,
        prevPage: prev,
        totalPages: totalPages,
      });
    });
  });
};

export const deleteReview = async (req, res) => {
  const uniqueEmployeeNumber = req.params.id;
  const checkQuery = `SELECT * FROM reviews WHERE Unique_employee_number = '${uniqueEmployeeNumber}'`;
  conn.query(checkQuery, (checkErr, checkResult) => {
    if (checkErr) {
      return res.status(500).json({ error: "Something went wrong" });
    }

    if (checkResult.length === 0) {
      return res.status(404).json({ error: "Review not found" });
    }

    const deleteQuery = `DELETE FROM reviews WHERE Unique_employee_number = '${uniqueEmployeeNumber}'`;
    conn.query(deleteQuery, (deleteErr, deleteResult) => {
      if (deleteErr) {
        return res.status(500).json({ error: "Something went wrong" });
      }
      return res.json({
        message: "Review deleted successfully",
        result: deleteResult,
      });
    });
  });
};

export const singup = async (req, res) => {
  conn.query(
    "SELECT `id`, `username` FROM `admins` WHERE `username` = '" +
      req.body.username +
      "'",
    (error, rows, fields) => {
      if (error) {
        console.log(error);
        return res.status(400).json(error);
      } else if (typeof rows !== "undefined" && rows.length) {
        return res.status(302).json({
          error: `The user with the name ${req.body.username} already exists.`,
        });
      } else {
        const name = req.body.username;
        const salt = bcrypt.genSaltSync(10);
        console.log(salt, req.body.password);
        const password = bcrypt.hashSync(req.body.password, salt);

        const newAdmin =
          "INSERT INTO `admins`(`username`,`password`) VALUES('" +
          name +
          "','" +
          password +
          "')";
        conn.query(newAdmin, (error, result) => {
          if (error) {
            res.status(302).json(error);
          } else {
            res.status(200).json(`Create new admin ${name}`);
          }
        });
      }
    }
  );
};
export const singin = async (req, res) => {
  conn.query(
    "SELECT `id`, `username`, `password` FROM `admins` WHERE `username` = '" +
      req.body.username +
      "'",
    (error, rows, fields) => {
      if (error) {
        return res.status(400).json(error);
      } else if (rows.length <= 0) {
        return res.status(401).json("Login or password is incorrect.");
      } else {
        const passwordIsValid = bcrypt.compareSync(
          req.body.password,
          rows[0].password
        );
        if (!passwordIsValid) {
          return res.status(401).json("Login or password is incorrect.");
        }
        const acceessToken = jwt.sign(
          TOKEN_USER_DATA(rows[0].id, rows[0].username),
          config.accessJWT,
          TOKEN_DATA.ACCESS
        );
        return res.status(200).json({
          id: rows[0].id,
          username: rows[0].username,
          accessToken: `Bearer ${acceessToken}`,
        });
      }
    }
  );
};
export const getUser = async (req, res) => {
  const { id } = req.params;
  conn.query(
    "SELECT `id`, `username` FROM `admins` WHERE `id` = '" + id + "'",
    (error, rows, fields) => {
      if (error) {
        return res.status(400).json(error);
      } else if (rows.length <= 0) {
        return res.status(404).json("User not found");
      } else {
        return res.status(200).json(rows[0]);
      }
    }
  );
};
// {
//   "username" : "Andrey",
//   "password": "123456"
// }
