import routers from "./route.js";

export default (app) => {
  app.use("/api", routers);
};
