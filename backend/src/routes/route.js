import Router from "express-promise-router";
import {
  deleteReview,
  getReviews,
  singup,
  singin,
  getUser,
} from "../controllers/controllers.js";
const routers = Router();

routers.get("/reviews/:page&:limit", getReviews);
routers.delete("/reviews/delete/:id", deleteReview);
routers.post("/auth/singup", singup);
routers.post("/auth/singin", singin);
routers.get("/user/:id", getUser);
export default routers;
