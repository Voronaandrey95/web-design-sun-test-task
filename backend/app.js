import express from "express";
import paginate from "express-paginate";
import mysql from "mysql";
import cors from "cors";
import bodyParser from "body-parser";
import routes from "./src/routes/index.js";
import { config } from "./config/config.js";
import cookieParser from "cookie-parser";

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(paginate.middleware(10, 50));
app.use(cookieParser());
export const conn = mysql.createConnection({
  host: config.HOST,
  user: config.DBUSER,
  password: config.DBPASSWORD,
  database: config.DBNAME,
});

conn.connect((err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("DB connected");
  }
});

routes(app);

app.listen(8000, () => console.log(`Server running on port 8000`));
