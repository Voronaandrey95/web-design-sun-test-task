export const COLORS = {
  BLUE: "#0880AE",
  ORANGE: "#F26532",
  DARK: "#252525",
  MAIN_TEXT: "#434343",
  SECCONDARY_TEXT: "#727272",
  MAIN_BG: "#FFFFFF",
  SECCONDARY_BG: "#F5F5F5",
};
