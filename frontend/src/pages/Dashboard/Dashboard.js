import MainConteiner from "../../components/MainContainer";
import MainSection from "../../components/MainSection/MainSection";
import { MainWrapper } from "./style";
import HeaderDashboard from "../../components/Dashboard/Header/HeaderDashboard";
import ReviewsSection from "../../components/Dashboard/ReviewsSection";
const Dashboard = () => {
  return (
    <MainWrapper>
      <HeaderDashboard></HeaderDashboard>
      <ReviewsSection />
    </MainWrapper>
  );
};

export default Dashboard;
