import { useState } from "react";
import { useAppDispatch } from "../../store";
import { loginAdmin } from "../../store/auth/actionCreators";
import {
  MainWrapper,
  FormWrapper,
  DateWrapper,
  // Label,
  DataInput,
  Title,
  SubmitButton,
} from "./style";
import { TextField } from "@mui/material";
const SingIn = () => {
  const dispatch = useAppDispatch();
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const onSuccess = () => {
    setError(false);
  };
  const onFailure = () => {
    setError(true);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(
      loginAdmin(
        {
          username: name,
          password: password,
        },
        onSuccess,
        onFailure
      )
    );
  };
  console.log(name);
  return (
    <MainWrapper>
      <FormWrapper onSubmit={handleSubmit}>
        <Title>SingIn</Title>
        <DateWrapper>
          <TextField
            error={error}
            id="outlined-error-helper-text"
            label="Login"
            defaultValue=""
            helperText={error ? "Login or password is not correct" : false}
            onChange={(e) => {
              error && onSuccess();
              setName(e.target.value);
            }}
          />
        </DateWrapper>
        <DateWrapper>
          <TextField
            error={error}
            id="outlined-error-helper-text"
            type="password"
            label="Password"
            defaultValue=""
            helperText={error ? "Login or password is not correct" : false}
            onChange={(e) => {
              error && onSuccess();
              setPassword(e.target.value);
            }}
          />
        </DateWrapper>
        <DateWrapper></DateWrapper>
        <SubmitButton>Login</SubmitButton>
      </FormWrapper>
    </MainWrapper>
  );
};

export default SingIn;
