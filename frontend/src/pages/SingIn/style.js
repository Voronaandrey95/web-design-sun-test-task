import { colors, styled } from "@mui/material";
import { COLORS } from "../../helpers/colors";

export const MainWrapper = styled("div")(({ theme }) => ({
  width: "100%",
  height: "100vh",
  backgroundColor: COLORS.BLUE,
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
}));
export const FormWrapper = styled("form")(({ theme }) => ({
  padding: 30,
  backgroundColor: COLORS.SECCONDARY_BG,
  width: 300,
  height: "fit-content",
  borderRadius: 30,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
}));

export const DateWrapper = styled("div")(({ theme }) => ({
  display: "flex",
  width: "100%",
  flexDirection: "column",
  marginBottom: 30,
}));
export const Title = styled("h1")(({ theme }) => ({
  color: COLORS.MAIN_TEXT,
}));
export const Label = styled("label")(({ theme }) => ({
  fontSize: 14,
}));
export const DataInput = styled("input")(({ theme }) => ({
  borderRadius: 5,
  border: `1px solid ${COLORS.DARK}`,
}));
export const SubmitButton = styled("button")(({ theme }) => ({
  padding: "10px",
  width: "100%",
  border: `1px solid ${COLORS.DARK}`,
  backgroundColor: COLORS.MAIN_BG,
  borderRadius: 20,
  cursor: "pointer",
}));
