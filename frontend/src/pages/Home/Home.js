import MainConteiner from "../../components/MainContainer";
import MainSection from "../../components/MainSection/MainSection";
const Home = () => {
  return (
    <>
      <MainConteiner>
        <MainSection />
      </MainConteiner>
    </>
  );
};

export default Home;
