import Endpoints from "../endpoints";
import { axiosInstance } from "../instance";

export const loging = (params) =>
  axiosInstance.post(Endpoints.AUTH.LOGIN, params);

export const getUser = (id, params) =>
  axiosInstance.get(Endpoints.AUTH.USER(id), params);

export const getReviews = (data) =>
  axiosInstance.get(Endpoints.AUTH.REVIEWS(data.page, data.limit));

export const deleteReviews = (id) =>
  axiosInstance.delete(Endpoints.AUTH.DELETE_REVIEWS(id));
