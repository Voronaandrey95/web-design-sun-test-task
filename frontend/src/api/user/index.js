import Endpoints from "../endpoints";
import { axiosInstance } from "../instance";

export const loging = (params) =>
  axiosInstance.post(Endpoints.AUTH.LOGIN, params);
