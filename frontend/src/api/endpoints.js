const api = "http://localhost:8000/api/";
const Endpoints = {
  AUTH: {
    LOGIN: `${api}auth/singin`,
    USER: (id) => `${api}user/${id}`,
    DELETE_REVIEWS: (id) => `${api}reviews/delete/${id}`,
    REVIEWS: (page, limit) => `${api}reviews/${page}&${limit}`,
  },
};
export default Endpoints;
