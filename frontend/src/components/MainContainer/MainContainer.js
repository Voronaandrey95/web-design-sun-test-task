import { StyledContainer, Container } from "./style";
import Header from "../Header";
import Footer from "../Footer";
import ComentSection from "../ComentSection/ComentSection";
const MainContainer = ({ children }) => {
  return (
    <>
      <Container>
        <Header />
        <StyledContainer>{children}</StyledContainer>
        <ComentSection />
        <Footer />
      </Container>
    </>
  );
};

export default MainContainer;
