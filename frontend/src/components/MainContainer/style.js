import { styled } from "@mui/material";

export const Container = styled("div")(({ theme }) => ({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
}));
export const StyledContainer = styled("div")(({ theme }) => ({
  maxWidth: 1320,
  padding: "0 20px",
  width: "fit-content",
}));
