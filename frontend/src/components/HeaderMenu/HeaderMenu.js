import { useRef, useState } from "react";
import {
  MainWrapper,
  TabWrapper,
  Tab,
  Lists,
  ListItem,
  ListsSub,
  ListName,
} from "./style";
import "./styleSVG.css";
import ArrowSVG from "../../helpers/svg/arrow";

const HeaderMenu = () => {
  const [openList, setOpenList] = useState(false);
  const [openSubList, setOpenSubList] = useState(false);
  const services = useRef();
  const handleMemu = () => {
    openList && setOpenSubList(false);
    setOpenList(() => !openList);
  };
  const handlSubList = () => {
    setOpenSubList(() => !openSubList);
  };
  const closeModalLeave = () => {
    if (openList) {
      setOpenList(false);
      setOpenSubList(false);
    }
  };
  const getTabs = () => {
    return (
      <>
        <TabWrapper>
          <Tab>Home</Tab>
        </TabWrapper>{" "}
        <TabWrapper ref={services} onMouseLeave={() => closeModalLeave()}>
          <div className="wrapperSVG">
            <Tab
              className={openList ? "active" : ""}
              onClick={() => handleMemu()}
            >
              Services
            </Tab>
            <ArrowSVG
              handleSubmit={handleMemu}
              marginLeft={5}
              rotate={openList ? 90 : -90}
              collor={openList ? "#0880AE" : "#252525"}
            />
          </div>
          {openList && (
            <Lists>
              <ListItem onClick={() => handleMemu()}>
                <ListName>Sub-Menu 1</ListName>
              </ListItem>
              <ListItem>
                <div className="wrapperSVG">
                  <ListName
                    onClick={() => handlSubList()}
                    className={openSubList ? "active" : ""}
                  >
                    Sub-Menu 2{" "}
                  </ListName>
                  <ArrowSVG
                    marginLeft={25}
                    handleSubmit={handlSubList}
                    rotate={openSubList ? 180 : 0}
                    collor={openSubList ? "#0880AE" : "#252525"}
                  />
                </div>

                {openSubList && (
                  <ListsSub>
                    <ListItem onClick={() => handleMemu()}>
                      <ListName>Turpis consectetur 3</ListName>
                    </ListItem>
                    <ListItem onClick={() => handleMemu()}>
                      <ListName>Senectus cursus pretium malesuada.</ListName>
                    </ListItem>
                    <ListItem onClick={() => handleMemu()}>
                      <ListName>Luctus neque frin 4</ListName>
                    </ListItem>
                  </ListsSub>
                )}
              </ListItem>

              <ListItem>
                <ListName>Turpis consectetur 3</ListName>
              </ListItem>
              <ListItem>
                <ListName>Turpis consectetur 3</ListName>
              </ListItem>
            </Lists>
          )}
        </TabWrapper>
        <TabWrapper>
          <div className="wrapperSVG">
            <Tab>About</Tab>
            <ArrowSVG marginLeft={5} rotate={-90} />
          </div>
        </TabWrapper>
        <TabWrapper>
          <Tab>Book now</Tab>
        </TabWrapper>
        <TabWrapper>
          <div className="wrapperSVG">
            <Tab>Shop</Tab>
            <ArrowSVG marginLeft={5} rotate={-90} />
          </div>
        </TabWrapper>
        <TabWrapper>
          <Tab>Blog</Tab>
        </TabWrapper>{" "}
        <TabWrapper>
          <Tab>Contact</Tab>
        </TabWrapper>
      </>
    );
  };
  return (
    <>
      <MainWrapper>{getTabs()}</MainWrapper>
    </>
  );
};

export default HeaderMenu;
