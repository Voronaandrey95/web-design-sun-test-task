import { styled } from "@mui/material";
import { COLORS } from "../../helpers/colors";
export const MainWrapper = styled("div")(({ theme }) => ({
  height: "100%",
  display: "flex",
  alignItems: "center",
}));
export const TabWrapper = styled("div")(({ theme }) => ({
  height: "100%",
  marginRight: 25,
  position: "relative",
  display: "flex",
  alignItems: "center",
}));

export const Tab = styled("p")(({ theme, className }) => ({
  textDecoration: "none",
  fontWeight: 500,
  height: "100%",
  display: "flex",
  alignItems: "center",
  color: className?.includes("active") ? COLORS.BLUE : COLORS.BLACK,
  wordWrap: "break-word",
  cursor: "pointer",
  "&:before": {
    display: className?.includes("active") ? "none" : "",
    position: "absolute",
    bottom: 0,
    content: "''",
    borderBottom: `3px solid ${COLORS.BLUE}`,
    transform: "scale(0,1)",
    width: "100%",
    transition: "transform 0.3s",
  },
  "&:hover:before": {
    transform: "scale(1,1)",
  },
  "&:hover": {
    color: `${COLORS.BLUE}`,
  },
}));
export const Lists = styled("ul")(({ theme }) => ({
  width: 175,
  transform: `translateY(100%)`,
  padding: 0,
  margin: 0,
  position: "absolute",
  bottom: 0,
  boxShadow: "0px 2px 8px rgba(0, 0, 0, 0.100361)",
  borderTop: `3px solid ${COLORS.BLUE}`,
  zIndex: 5,
}));
export const ListItem = styled("li")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  position: "relative",
  listStyleType: "none",
  padding: "12px 5px 12px 14px",
  borderBottom: `1px solid ${COLORS.SECCONDARY_BG}`,
  background: COLORS.MAIN_BG,
}));
export const ListName = styled("p")(({ theme, className }) => ({
  fontWeight: 500,
  color: className?.includes("active") ? COLORS.BLUE : COLORS.BLACK,
  cursor: "pointer",
  display: "flex",
  margin: 0,
  padding: 0,
  fontSize: 14,
  "&:hover": {
    color: `${COLORS.BLUE}`,
  },
}));
export const ListsSub = styled("ul")(({ theme }) => ({
  width: 175,
  padding: 0,
  margin: 0,
  position: "absolute",
  top: 0,
  left: 175,
  boxShadow: "0px 2px 8px rgba(0, 0, 0, 0.100361)",
  borderTop: `3px solid ${COLORS.BLUE}`,
}));
