import { styled } from "@mui/material";
import { tabletBreakpoint } from "../../helpers/breakpointCalendar";
import { COLORS } from "../../helpers/colors";
export const MainWrapper = styled("div")(({ theme }) => ({
  background: COLORS.MAIN_TEXT,
  width: "100%",
  marginTop: 80,
  width: "100%",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  ...tabletBreakpoint({}),
}));

export const FooterWrapper = styled("div")(({ theme }) => ({
  maxWidth: 1320,
  width: "-webkit-fill-available",
  padding: "64px 20px 44px 20px",
  display: "grid",
  gridTemplateColumns: "repeat(4, 1fr)",
  gridTemplateRows: "1fr",
  gridColumnGap: "20px",
  gridRowGap: "12px",
  ...tabletBreakpoint({
    gridTemplateColumns: "repeat(2, 1fr)",
  }),
}));
export const FooterIformation = styled("div")(({ theme }) => ({
  width: "100%",
  display: "flex",
  flexDirection: "column",
  marginBottom: 40,
  ...tabletBreakpoint({
    paddingTop: 10,
    borderBottom: `2px solid ${COLORS.SECCONDARY_BG}`,
    borderTop: `2px solid ${COLORS.SECCONDARY_BG}`,
  }),
}));
export const FooterTitle = styled("p")(({ theme }) => ({
  margin: 0,
  fontWeight: 500,
  color: COLORS.MAIN_BG,
  fontSize: 22,
  lineHeight: "40px",
  marginBottom: 24,
}));
export const FooterLink = styled("a")(({ theme }) => ({
  margin: 0,
  fontWeight: 400,
  fontSize: 14,
  color: COLORS.MAIN_BG,
  lineHeight: "40px",
  lineHeight: "24px",
  marginBottom: 8,
}));
export const FooterText = styled("p")(({ theme }) => ({
  margin: 0,
  fontWeight: 400,
  fontSize: 14,
  color: COLORS.MAIN_BG,
  lineHeight: "40px",
  lineHeight: "28px",
  marginTop: 20,
}));
