import {
  MainWrapper,
  FooterWrapper,
  FooterIformation,
  FooterTitle,
  FooterLink,
  FooterText,
} from "./style";
import { Link } from "react-router-dom";
const Footer = () => {
  return (
    <>
      <MainWrapper>
        <FooterWrapper>
          <FooterIformation>
            <FooterTitle>Departments</FooterTitle>
            <FooterLink href="#">Medical</FooterLink>
            <FooterLink href="#">Pharmaceuticals</FooterLink>
            <FooterLink href="#">Medical Equipment</FooterLink>
          </FooterIformation>
          <FooterIformation>
            <FooterTitle>Quick Links</FooterTitle>
            <FooterLink href="#">What do we do?</FooterLink>
            <FooterLink href="#">Our expertise</FooterLink>
            <FooterLink href="#">Request an Appointment</FooterLink>
            <FooterLink href="#">Book with a Specialist</FooterLink>
          </FooterIformation>
          <FooterIformation>
            <FooterTitle>Head Office</FooterTitle>
            <div style={{ display: "flex" }}>
              <img
                style={{
                  width: 9,
                  height: 12,
                  marginRight: 8,
                  cursor: "pointer",
                  marginTop: 6,
                }}
                src="./icons/location.svg"
                alt="logo"
              ></img>
              <FooterLink href="#">
                4517 Washington Ave. Manchester, Kentucky 39495
              </FooterLink>
            </div>
            <div>
              <img
                style={{
                  width: 9,
                  height: 12,
                  marginRight: 8,
                  cursor: "pointer",
                }}
                src="./icons/mail.svg"
                alt="logo"
              ></img>
              <FooterLink href="mailto:darrell@mail.com">
                darrell@mail.com
              </FooterLink>
            </div>
            <div>
              <img
                style={{
                  width: 9,
                  height: 12,
                  marginRight: 8,
                  cursor: "pointer",
                }}
                src="./icons/phone.svg"
                alt="logo"
              ></img>
              <FooterLink href="tel:671-555-0110">(671) 555-0110</FooterLink>
            </div>
          </FooterIformation>
          <FooterIformation>
            <img
              style={{
                width: "100%",
                height: 122,
                cursor: "pointer",
              }}
              src="./icons/logo2.svg"
              alt="logo"
            ></img>
            <FooterText>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras
              blandit tincidunt ut sed. Velit euismod integer convallis ornare
              eu.
            </FooterText>
          </FooterIformation>
          <FooterText>©2021 All Rights Reserved</FooterText>
        </FooterWrapper>
      </MainWrapper>
    </>
  );
};

export default Footer;
