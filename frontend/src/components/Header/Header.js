import {
  InfoSection,
  MainWrapper,
  ContactWrapper,
  HelpLink,
  HeaderWrapper,
} from "./style";
import { Link } from "react-router-dom";
import HeaderMenu from "../HeaderMenu/HeaderMenu";
import { COLORS } from "../../helpers/colors";
const Header = () => {
  return (
    <>
      <MainWrapper>
        <HeaderWrapper>
          <InfoSection>
            <Link to="*">
              <img
                style={{
                  width: 140,
                  height: 56,
                  marginRight: 40,
                  cursor: "pointer",
                }}
                src="./image/logo.svg"
                alt="logo"
              ></img>
            </Link>
            <ContactWrapper>
              <Link
                to="*"
                style={{
                  color: COLORS.SECCONDARY_TEXT,
                  fontWeight: 400,
                  fontSize: 12,
                  marginBottom: 8,
                }}
              >
                Need Help?
              </Link>
              <a
                href="tel:514-543-9936"
                style={{
                  fontSize: 22,
                  color: COLORS.DARK,
                }}
              >
                (514) 543-9936
              </a>
            </ContactWrapper>
          </InfoSection>
          <HeaderMenu />
        </HeaderWrapper>
      </MainWrapper>
    </>
  );
};

export default Header;
