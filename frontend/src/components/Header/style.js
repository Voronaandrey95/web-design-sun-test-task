import { styled } from "@mui/material";
import { tabletBreakpoint } from "../../helpers/breakpointCalendar";
export const MainWrapper = styled("div")(({ theme }) => ({
  width: "100%",
  display: "flex",
  height: 96,
  flexDirection: "column",
  alignItems: "center",
  boxShadow: " 0px 1px 8px rgba(0, 0, 0, 0.100361)",
  ...tabletBreakpoint({
    justifyContent: "space-around",
  }),
}));

export const HeaderWrapper = styled("div")(({ theme }) => ({
  maxWidth: 1320,
  width: "-webkit-fill-available",
  padding: "0 20px",
  display: "flex",
  justifyContent: "space-between",
  ...tabletBreakpoint({
    justifyContent: "center",
  }),
}));

export const StyledContainer = styled("div")(({ theme }) => ({}));
export const InfoSection = styled("div")(({ theme }) => ({
  display: "flex",
  height: 96,
  alignItems: "center",
  ...tabletBreakpoint({
    display: "none",
  }),
}));
export const ContactWrapper = styled("div")(({ theme }) => ({
  display: "flex",
  flexDirection: "column",
}));
export const HelpLink = styled("a")(({ theme }) => ({}));
