import {
  MainWrapper,
  BlogWrapper,
  BlogImg,
  BlogArticle,
  BlogName,
  BlogTitle,
  BlogText,
  InfoSection,
  Date,
  Author,
  Time,
  NewsWrapper,
  NewsTitle,
  NewsPostsWrapper,
  NewsPostWrapper,
  PostTitle,
  PostDate,
} from "./style";

const MainSection = () => {
  return (
    <MainWrapper>
      <BlogWrapper>
        <BlogImg></BlogImg>
        <BlogArticle>
          <BlogName>Pharmaceuticals</BlogName>
          <BlogTitle>A Sure Way To Get Rid Of Your Back Ache Problem</BlogTitle>
          <BlogText>
            If you have tried everything, but still seem to suffer from snoring,
            don’t give up. Before turning to surgery, consider shopping for
            anti-snore devices. These products do not typically require a
            prescription, are economically priced and may just be the answer
            that you are looking for. However, as is the case when shopping for
            anything, there are a lot of anti-snore devices out there and…{" "}
          </BlogText>
          <InfoSection>
            <Date>28 Feb 2021</Date>
            <Author>Jim Sullivan</Author>
            <Time>
              <img
                style={{
                  width: 12,
                  height: 12,
                  marginRight: 4,
                }}
                src="./icons/clock.svg"
                alt="clock"
              ></img>
              <Date>6 min read</Date>
            </Time>
          </InfoSection>
        </BlogArticle>
      </BlogWrapper>
      <NewsWrapper>
        <NewsTitle>Our Latest News</NewsTitle>
        <NewsPostsWrapper>
          <NewsPostWrapper url={"./image/massage.webp"}>
            <PostTitle>Basic Swedish Back Massage Techniques</PostTitle>
            <PostDate>28 Feb 2021</PostDate>
          </NewsPostWrapper>
          <NewsPostWrapper url={"./image/code_laptop.webp"}>
            <PostTitle>How to Learn Coding for Beginners</PostTitle>
            <PostDate>28 Feb 2021</PostDate>
          </NewsPostWrapper>
          <NewsPostWrapper url={"./image/people_working.webp"}>
            <PostTitle>Google’s Influence Over Think Tanks</PostTitle>
            <PostDate>28 Feb 2021</PostDate>
          </NewsPostWrapper>
        </NewsPostsWrapper>
      </NewsWrapper>
    </MainWrapper>
  );
};

export default MainSection;
