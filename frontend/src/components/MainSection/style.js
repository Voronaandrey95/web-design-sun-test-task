import { styled } from "@mui/material";
import { COLORS } from "../../helpers/colors";
import { tabletBreakpoint } from "../../helpers/breakpointCalendar";
export const Container = styled("div")(({ theme }) => ({}));
export const MainWrapper = styled("div")(({ theme }) => ({
  marginTop: 50,
  marginBottom: 80,
  display: "grid",
  gridTemplateColumns: "2fr 1fr",
  gridTemplateRows: "1fr",
  gridColumnGap: "25px",
  gridRowGap: " 0px",
  ...tabletBreakpoint({
    gridTemplateColumns: "1fr",
    gridTemplateRows: "repeat(2,1fr)",
    gridColumnGap: "0px",
    gridRowGap: "12px",
  }),
}));
export const BlogWrapper = styled("div")(({ theme }) => ({
  display: "grid",
  gridTemplateColumns: "repeat(2, 1fr)",
  gridTemplateRows: "1fr",
  gridColumnGap: "0px",
  gridRowGap: " 0px",
}));
export const BlogImg = styled("div")(({ theme }) => ({
  background: "no-repeat center",
  backgroundImage: "url(./image/yoga_studio.webp)",
  backgroundSize: "cover",
  width: "100%",
  height: "100%",
}));
export const BlogArticle = styled("div")(({ theme }) => ({
  padding: "32px 24px",
  backgroundColor: COLORS.SECCONDARY_BG,
  ...tabletBreakpoint({
    textAling: "center",
  }),
}));
export const BlogName = styled("p")(({ theme }) => ({
  color: COLORS.BLUE,
  fontSize: 14,
  fontWeight: 700,
  margin: 0,
}));
export const BlogTitle = styled("h1")(({ theme }) => ({
  color: COLORS.DARK,
  fontSize: 22,
  fontWeight: 500,
  margin: 0,
  lineHeight: "40px",
  marginTop: 8,
}));
export const BlogText = styled("h1")(({ theme }) => ({
  color: COLORS.MAIN_TEXT,
  fontSize: 14,
  fontWeight: 500,
  margin: 0,
  lineHeight: "28px",
  marginTop: 8,
}));
export const InfoSection = styled("div")(({ theme }) => ({
  marginTop: 35,
  display: "flex",
  alignItems: "center",
}));
export const Date = styled("p")(({ theme }) => ({
  color: COLORS.SECCONDARY_TEXT,
  fontSize: 12,
  fontWeight: 400,
  margin: 0,
  lineHeight: "16px",
}));
export const Author = styled("p")(({ theme }) => ({
  color: COLORS.BLUE,
  fontSize: 12,
  fontWeight: 400,
  margin: "0 0 0 8px",
  lineHeight: "16px",
  cursor: "pointer",
}));
export const Time = styled("div")(({ theme }) => ({
  marginLeft: 32,
  display: "flex",
  alignItems: "center",
}));
export const NewsWrapper = styled("div")(({ theme }) => ({
  display: "flex",
  flexDirection: "column",
}));
export const NewsTitle = styled("div")(({ theme }) => ({
  color: COLORS.DARK,
  fontSize: 30,
  fontWeight: 500,
  marginBottom: 18,
  lineHeight: "46px",
}));
export const NewsPostsWrapper = styled("div")(({ theme }) => ({
  display: "grid",
  gridTemplateColumns: "1fr",
  gridTemplateRows: "repeat(3, 1fr)",
  gridColumnGap: "0px",
  gridRowGap: " 12px",
  height: "100%",
}));
export const NewsPostWrapper = styled("div")(({ theme, url }) => ({
  width: "100%",
  height: "100%",
  background: "no-repeat center",
  backgroundImage: `url(${url})`,
  backgroundSize: "cover",
  cursor: "pointer",
  position: "relative",
  "&:before": {
    content: "''",
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: " 100%",
    backgroundColor: "rgba(0, 0, 0, 0.6)",
    zIndex: 1,
    transition: "background-color 0.3s",
  },
  "&:hover:before": {
    backgroundColor: "rgba(8, 128, 174, 0.8)",
    zIndex: 2,
  },
}));
export const PostTitle = styled("p")(({ theme }) => ({
  color: COLORS.MAIN_BG,
  position: "absolute",
  fontSize: 18,
  margin: 0,
  fontWeight: 500,
  lineHeight: "27px",
  zIndex: 3,
  top: 17,
  left: 32,
}));
export const PostDate = styled("p")(({ theme }) => ({
  color: COLORS.MAIN_BG,
  position: "absolute",
  fontSize: 12,
  margin: 0,
  fontWeight: 500,
  lineHeight: "16px",
  zIndex: 3,
  left: 32,
  bottom: 17,
}));
