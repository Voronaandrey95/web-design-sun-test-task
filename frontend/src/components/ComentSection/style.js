import { styled } from "@mui/material";
import {
  tabletBreakpoint,
  smallTabletBreakpoint,
} from "../../helpers/breakpointCalendar";
import { COLORS } from "../../helpers/colors";
export const MainWrapper = styled("div")(({ theme }) => ({
  background: COLORS.SECCONDARY_BG,
  width: "100%",
  width: "100%",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
}));

export const MainCommentWrapper = styled("div")(({ theme }) => ({
  maxWidth: 1320,
  padding: "80px 20px 44px 20px",
  display: "flex",
  width: "-webkit-fill-available",
  flexDirection: "column",
  alignItems: "center",
}));

export const CommentTitle = styled("p")(({ theme }) => ({
  margin: 0,
  fontWeight: 500,
  fontSize: 36,
  color: COLORS.DARK,
  lineHeight: "56px",
  marginBottom: 40,
}));
export const CommentSectionWrapper = styled("div")(({ theme }) => ({
  display: "flex",
  flexDirection: "column",
}));
export const CommentWrapper = styled("div")(({ theme }) => ({
  display: "flex",
  background: COLORS.MAIN_BG,
  border: `1px solid #C1C7D0`,
  flexDirection: "column",
  maxWidth: 200,
  height: 320,
  padding: "20px 24px",
  clipPath: "polygon(0% 0%, 100% 0%, 100% 95%, 8% 95%, 0 100%, 0 92%)",
}));
export const Title = styled("div")(({ theme }) => ({
  fontWeight: 600,
  fontSize: 14,
  lineHeight: "28px",
  color: COLORS.DARK,
  marginBottom: 2,
}));
export const SubTitle = styled("div")(({ theme }) => ({
  fontWeight: 500,
  fontSize: 12,
  lineHeight: "16px",
  color: COLORS.DARK,
  marginBottom: 10,
}));
export const Text = styled("div")(({ theme }) => ({
  fontWeight: 400,
  fontSize: 14,
  lineHeight: "28px",
  color: COLORS.DARK,
  marginBottom: 2,
  fontStyle: "italic",
}));
export const AuthorWrapper = styled("div")(({ theme }) => ({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
}));
export const UserName = styled("p")(({ theme }) => ({
  margin: 0,
  fontWeight: 500,
  fontSize: 14,
  lineHeight: "150%",
  color: COLORS.DARK,
}));
export const GridWrapper = styled("div")(({ theme }) => ({
  display: "grid",
  gridTemplateColumns: "repeat(5, 1fr)",
  gridTemplateRows: "1fr",
  gridColumnGap: "24px",
  gridRowGap: "44px",
  ...tabletBreakpoint({
    gridTemplateColumns: "repeat(4, 1fr)",
  }),
  ...smallTabletBreakpoint({
    gridTemplateColumns: "repeat(3, 1fr)",
  }),
}));
