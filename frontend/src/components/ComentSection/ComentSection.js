import {
  MainWrapper,
  MainCommentWrapper,
  CommentTitle,
  CommentSectionWrapper,
  CommentWrapper,
  SubTitle,
  Text,
  Title,
  AuthorWrapper,
  UserName,
  GridWrapper,
} from "./style";
import { Rating } from "@mui/material";
import { Pagination } from "@mui/material";
import { useEffect, useState } from "react";
import { useAppDispatch } from "../../store";
import { getReviews } from "../../store/reviews/actionCreators";
import { useSelector } from "react-redux";

const ComentSection = () => {
  const dispatch = useAppDispatch();
  const { data, currentPage, totalPages } = useSelector(
    (state) => state.reviews?.reviewsData
  );
  const [page, setPage] = useState(1);
  const [limit, setLimit] = useState(15);
  useEffect(() => {
    setPage(currentPage);
  }, [currentPage]);

  useEffect(() => {
    dispatch(
      getReviews({
        page: page,
        limit: limit,
      })
    );
  }, [page]);
  const getComment = (data) => {
    return (
      <CommentSectionWrapper>
        <CommentWrapper>
          <Title>{data.Employee}</Title>
          <SubTitle>{data.Employees_position}</SubTitle>
          <Text>{data.Review}</Text>
        </CommentWrapper>
        <AuthorWrapper>
          {" "}
          <img
            style={{
              width: 50,
              height: 50,
              marginRight: 12,
              cursor: "pointer",
              marginTop: 6,
            }}
            src="./icons/Avatar.svg"
            alt="logo"
          ></img>
          <div>
            <UserName>{data.Reviewer}</UserName>
            <Rating
              name="half-rating-read"
              defaultValue={data.Rating}
              precision={0.1}
              readOnly
            />
          </div>
        </AuthorWrapper>
      </CommentSectionWrapper>
    );
  };
  return (
    <>
      <MainWrapper>
        <MainCommentWrapper>
          <CommentTitle>Testimonials</CommentTitle>
          <GridWrapper>
            {data.map((e) => {
              return getComment(e);
            })}
          </GridWrapper>
          <Pagination
            defaultPage={currentPage}
            color="primary"
            sx={{
              display: "flex",
              justifyContent: "flex-end",
              marginTop: "60px",
              "li button": {
                color: "#727272",
                background: "#FFFFFF",
                border: "1px solid #0880AE",
              },
            }}
            page={currentPage}
            count={totalPages}
            onChange={(event, page) => {
              setPage(page);
            }}
          />
        </MainCommentWrapper>
      </MainWrapper>
    </>
  );
};

export default ComentSection;
