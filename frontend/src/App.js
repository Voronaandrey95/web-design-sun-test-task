import React from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import { useSelector } from "react-redux";
import { UserRoutes } from "./helpers/routes";
import Dashboard from "./pages/Dashboard/Dashboard";
import Home from "./pages/Home/Home";
import SingIn from "./pages/SingIn/SingIn";
import NotificationProvider from "./components/notification/notificationContext";
const App = () => {
  const isLoggedIn = useSelector((state) => !!state.auth.authData.accessToken);

  return (
    <Router>
      <NotificationProvider>
        <Routes>
          {isLoggedIn && (
            <>
              <Route path={UserRoutes.Dashboard} element={<Dashboard />} />
              <Route
                path={UserRoutes.Login}
                element={<Navigate replace to={UserRoutes.Dashboard} />}
              />
            </>
          )}
          {!isLoggedIn && (
            <>
              <Route path={UserRoutes.Login} element={<SingIn />} />
              <Route
                path={UserRoutes.Dashboard}
                element={<Navigate replace to={UserRoutes.Login} />}
              />
            </>
          )}
          <Route path={UserRoutes.Home} element={<Home />} />
          <Route path="/" element={<Navigate replace to={UserRoutes.Home} />} />
          <Route path="*" element={<Navigate replace to={UserRoutes.Home} />} />
        </Routes>
      </NotificationProvider>
    </Router>
  );
};

export default App;
