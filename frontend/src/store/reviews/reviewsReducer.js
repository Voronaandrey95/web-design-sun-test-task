import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  reviewsData: {
    data: [],
    currentPage: 1,
    prevPage: null,
    nextPage: 2,
    totalPages: 2,
    isLoading: false,
    error: null,
  },
};
export const reviewsReducer = createSlice({
  name: "reviews",
  initialState,
  reducers: {
    reviewsStart: (state) => {
      return {
        ...state,
        reviewsData: {
          ...state.reviewsData,
          isLoading: true,
        },
      };
    },
    reviewsSuccess: (state, action) => {
      return {
        ...state,
        reviewsData: {
          ...state.reviewsData,
          data: action.payload.data.data,
          currentPage: action.payload.data.currentPage,
          prevPage: action.payload.data.prevPage,
          nextPage: action.payload.data.nextPage,
          totalPages: action.payload.data.totalPages,
          isLoading: false,
          error: null,
        },
      };
    },
    reviewsFailure: (state, action) => {
      return {
        ...state,
        reviewsData: {
          ...state.reviewsData,
          isLoading: false,
          error: action.payload,
        },
      };
    },
    deleteReviewsStart: (state) => {
      return {
        ...state,
        reviewsData: {
          ...state.reviewsData,
          isLoading: true,
        },
      };
    },
    deleteReviewsSuccess: (state) => {
      return {
        ...state,
        reviewsData: {
          ...state.reviewsData,
          isLoading: false,
          error: null,
        },
      };
    },
    deleteReviewsFailure: (state, action) => {
      return {
        ...state,
        reviewsData: {
          ...state.reviewsData,
          isLoading: false,
          error: action.payload,
        },
      };
    },
  },
});

export const {
  reviewsStart,
  reviewsSuccess,
  reviewsFailure,
  deleteReviewsFailure,
  deleteReviewsStart,
  deleteReviewsSuccess,
} = reviewsReducer.actions;
export default reviewsReducer.reducer;
