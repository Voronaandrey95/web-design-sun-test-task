import api from "../../api";
import {
  reviewsStart,
  reviewsFailure,
  reviewsSuccess,
  deleteReviewsFailure,
  deleteReviewsStart,
  deleteReviewsSuccess,
} from "./reviewsReducer";

export const getReviews = (data) => async (dispatch) => {
  try {
    dispatch(reviewsStart());
    const res = await api.auth.getReviews(data);
    dispatch(reviewsSuccess(res));
  } catch (error) {
    dispatch(reviewsFailure(error.massage));
  }
};
export const deleteReviews = (id, onSuccess, onFailure) => async (dispatch) => {
  try {
    dispatch(deleteReviewsSuccess());
    const res = await api.auth.deleteReviews(id);
    dispatch(deleteReviewsStart(res));
    onSuccess();
  } catch (error) {
    onFailure(error.massage);
    dispatch(deleteReviewsFailure(error.massage));
  }
};
