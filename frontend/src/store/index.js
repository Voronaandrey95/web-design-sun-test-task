import {
  configureStore,
  getDefaultMiddleware,
  combineReducers,
} from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";
import { authReducer } from "./auth/authReducer";
import { reviewsReducer } from "./reviews/reviewsReducer";
import { userReducer } from "./user/userReducer";

const rootReducer = combineReducers({
  auth: authReducer.reducer,
  user: userReducer.reducer,
  reviews: reviewsReducer.reducer,
});

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
export const useAppDispatch = useDispatch;
